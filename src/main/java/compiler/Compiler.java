package main.java.compiler;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import main.java.antlr.SimpleCLexer;
import main.java.antlr.SimpleCParser;
import main.java.ast.PrettyPrinterVisitor;

public class Compiler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String testString = "int helloWorld(int argc, unsigned int argv){return;}";

		SimpleCLexer lexer = new SimpleCLexer(CharStreams.fromString(testString));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		SimpleCParser parser = new SimpleCParser(tokens);
		ParseTree tree = parser.translationUnit();
		PrettyPrinterVisitor pp = new PrettyPrinterVisitor();
		System.out.println(pp.visit(tree));

	}

}
