package main.java.ir.core;

import java.util.ArrayList;
import java.util.List;

public class IRValue {

	private List<IROperation> uses; /* !< List of operations that use the value as operand */
	private IRType type; /* !< Type of the value. Set when building the value. */

	public IRValue(IRType type) {
		this.setType(type);
		this.uses = new ArrayList<IROperation>();
	}

	public List<IROperation> getUses() {
		return uses;
	}

	public void setUses(List<IROperation> uses) {
		this.uses = uses;
	}

	public IRType getType() {
		return type;
	}

	public void setType(IRType type) {
		this.type = type;
	}
}
