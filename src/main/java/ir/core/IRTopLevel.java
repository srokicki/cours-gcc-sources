package main.java.ir.core;

import java.util.ArrayList;
import java.util.List;

public class IRTopLevel implements IRVisitableObject {

	private List<IRFunction> funcs; /* !< List of functions contained in the top level */

	// Class builder
	public IRTopLevel() {
		this.setFuncs(new ArrayList<IRFunction>());
	}

	// ********************************************************
	// Getters and setters

	public List<IRFunction> getFuncs() {
		return funcs;
	}

	public void setFuncs(List<IRFunction> funcs) {
		this.funcs = funcs;
	}

	// ********************************************************
	// For visitor

	@Override
	public Object accept(IRVisitor v) {
		return v.visitTopLevel(this);
	}

}
