package main.java.ir.core;

import main.java.ir.instruction.IRAddInstruction;
import main.java.ir.instruction.IRAllocaInstruction;
import main.java.ir.instruction.IRGetElementPtrInstruction;
import main.java.ir.instruction.IRLoadInstr;
import main.java.ir.instruction.IRMulInstruction;
import main.java.ir.instruction.IRStoreInstruction;
import main.java.ir.terminator.IRCondBr;
import main.java.ir.terminator.IRGoto;

public abstract class IRVisitor<T> {

	public abstract T visitTopLevel(IRTopLevel t);

	public abstract T visitFunction(IRFunction f);

	public abstract T visitBlock(IRBlock b);

	public abstract T visitPhiOperation(IRPhiOperation p);

	public abstract T visitGotoTerminator(IRGoto g);

	public abstract T visitCondBr(IRCondBr b);

	public abstract T visitAddInstruction(IRAddInstruction i);

	public abstract T visitMulInstruction(IRMulInstruction m);

	public abstract T visitAllocaInstruction(IRAllocaInstruction i);

	public abstract T visitGetElementPtrInstruction(IRGetElementPtrInstruction i);

	public abstract T visitLoadInstruction(IRLoadInstr i);

	public abstract T visitStoreInstruction(IRStoreInstruction i);

}
