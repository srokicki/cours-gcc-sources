package main.java.ir.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.java.ir.terminator.IRTerminator;

public class IRBlock implements IRVisitableObject {

	private List<IROperation> operations; /*
											 * !< List of operations inside the block. Last one should be a IRTerminator
											 */
	private List<IRBlock> predecessors; /*
										 * !< List of predecessors in the control flow graph. Built automatically when
										 * calling addTerminator() on a block
										 */

	public IRBlock() {
		this.setOperations(new ArrayList<IROperation>());
		this.setPredecessors(new ArrayList<IRBlock>());
	}

	public IRTerminator getTerminator() {
		assert (this.operations.get(this.operations.size() - 1) instanceof IRTerminator);
		return (IRTerminator) this.operations.get(this.operations.size() - 1);
	}

	public void addTerminator(IRTerminator t) {
		// We add predecessor to each successor
		for (IRBlock succ : t.getSuccessors()) {
			succ.getPredecessors().add(this);
		}
		// We insert the terminator operation
		this.operations.add(t);
	}

	public Iterator<IRBlock> getSuccessors() {
		return this.getTerminator().getSuccessors().iterator();
	}

	// ********************************************************
	// Getters and setters

	public List<IRBlock> getPredecessors() {
		return predecessors;
	}

	public void setPredecessors(List<IRBlock> predecessors) {
		this.predecessors = predecessors;
	}

	public List<IROperation> getOperations() {
		return operations;
	}

	public void setOperations(List<IROperation> operations) {
		this.operations = operations;
	}

	// ********************************************************
	// For visitor

	@Override
	public Object accept(IRVisitor v) {
		return v.visitBlock(this);
	}

}
