package main.java.ir.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class IROperation implements IRVisitableObject {

	private IRValue result; /* !< Value representing the result of the operation */
	private List<IRValue> operands; /* !< List of values representing the operands of the operation */

	protected IROperation() {
		this.operands = new ArrayList<IRValue>();
	}

	public void addOperand(IRValue v) {
		this.operands.add(v);
		v.getUses().add(this);
	}

	// ********************************************************
	// Getters and setters

	public IRValue getResult() {
		return result;
	}

	public void setResult(IRValue result) {
		this.result = result;
	}

	public Iterator<IRValue> getOperands() {
		return operands.iterator();
	}

	public void setOperands(List<IRValue> operands) {
		this.operands = operands;
	}
}
