package main.java.ir.core;

import java.util.ArrayList;
import java.util.List;

public class IRFunction implements IRVisitableObject {

	private String name; /* !< Function name */
	private List<IRBlock> blocks; /* !< Basic blocks belonging to the function */

	public IRFunction(String name) {
		this.setName(name);
		this.setBlocks(new ArrayList<IRBlock>());
	}

	public void addBlock(IRBlock block) {
		this.blocks.add(block);
	}

	// ********************************************************
	// Getters and setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<IRBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<IRBlock> blocks) {
		this.blocks = blocks;
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitFunction(this);
	}
}
