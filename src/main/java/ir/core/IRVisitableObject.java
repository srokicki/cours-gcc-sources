package main.java.ir.core;

public interface IRVisitableObject<T> {

	public T accept(IRVisitor v);
}
