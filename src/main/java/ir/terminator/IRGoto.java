package main.java.ir.terminator;

import main.java.ir.core.IRBlock;
import main.java.ir.core.IRVisitor;

public class IRGoto extends IRTerminator {

	// IRGoto
	// One successor, no operand

	public IRGoto(IRBlock dest) {
		super();
		this.getSuccessors().add(dest);
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitGotoTerminator(this);
	}
}
