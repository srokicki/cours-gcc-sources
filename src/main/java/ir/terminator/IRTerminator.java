package main.java.ir.terminator;

import java.util.ArrayList;
import java.util.List;

import main.java.ir.core.IRBlock;
import main.java.ir.core.IROperation;

public abstract class IRTerminator extends IROperation {

	private List<IRBlock> successors;

	protected IRTerminator() {
		super();
		this.successors = new ArrayList<IRBlock>();
	}

	public List<IRBlock> getSuccessors() {
		return successors;
	}

	public void setSuccessors(List<IRBlock> successors) {
		this.successors = successors;
	}

}
