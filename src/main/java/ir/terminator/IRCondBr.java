package main.java.ir.terminator;

import main.java.ir.core.IRBlock;
import main.java.ir.core.IRValue;
import main.java.ir.core.IRVisitor;

public class IRCondBr extends IRTerminator {

	// IRCondBr
	// Two successors, an operands to verify whether we take first succ (if true) or
	// second succ (if false)

	public IRCondBr(IRValue cond, IRBlock ifTrue, IRBlock ifFalse) {
		super();
		this.addOperand(cond);
		this.getSuccessors().add(ifTrue);
		this.getSuccessors().add(ifFalse);
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitCondBr(this);
	}
}
