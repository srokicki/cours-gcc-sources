package main.java.ir.instruction;

import main.java.ir.core.IRValue;
import main.java.ir.core.IRVisitor;

public class IRStoreInstruction extends IRInstruction {

	// Store instructions has one address operand and a value operand
	public IRStoreInstruction(IRValue addr, IRValue val) {
		super();
		this.addOperand(addr);
		this.addOperand(val);
		this.setResult(null);
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitStoreInstruction(this);
	}
}
