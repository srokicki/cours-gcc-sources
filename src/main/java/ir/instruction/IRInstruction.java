package main.java.ir.instruction;

import main.java.ir.core.IROperation;
import main.java.ir.core.IRType;

public abstract class IRInstruction extends IROperation {

	protected IRInstruction() {
		super();
	}

	// Used to get the result type of a binary instruction
	protected IRType getBinaryOpResultType(IRType type1, IRType type2) {
		if (type1 == IRType.FLOAT || type2 == IRType.FLOAT) {
			return IRType.FLOAT;
		} else if (type1 == IRType.CHAR && type2 == IRType.CHAR) {
			return IRType.CHAR;
		} else if (type1 == IRType.UINT && type2 == IRType.UINT) {
			return IRType.UINT;
		} else {
			return IRType.INT;
		}
	}
}
