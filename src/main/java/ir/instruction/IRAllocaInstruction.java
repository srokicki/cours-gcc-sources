package main.java.ir.instruction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import main.java.ir.core.IRType;
import main.java.ir.core.IRVisitor;

public class IRAllocaInstruction extends IRInstruction {

	private String varName = "";
	private int size;
	private IRType type;
	private List<IRGetElementPtrInstruction> uses;

	public IRAllocaInstruction(String name, IRType type, int size) {
		super();
		this.setVarName(name);
		this.setType(type);
		this.setSize(size);
		this.uses = new ArrayList<IRGetElementPtrInstruction>();
	}

	public void addUse(IRGetElementPtrInstruction elPtr) {
		this.uses.add(elPtr);
	}

	public IRType getType() {
		return type;
	}

	public void setType(IRType type) {
		this.type = type;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public Iterator<IRGetElementPtrInstruction> getUses() {
		return uses.iterator();
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitAllocaInstruction(this);
	}

}
