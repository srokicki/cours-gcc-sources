package main.java.ir.instruction;

import main.java.ir.core.IRType;
import main.java.ir.core.IRValue;
import main.java.ir.core.IRVisitor;

public class IRGetElementPtrInstruction extends IRInstruction {

	// GetElementPtr returns the address of a variable
	private IRAllocaInstruction def;

	public IRGetElementPtrInstruction(IRAllocaInstruction alloca) {
		super();
		this.def = alloca;
		alloca.addUse(this);
		this.setResult(new IRValue(IRType.UINT));
	}

	public IRAllocaInstruction getDef() {
		return def;
	}

	public void setDef(IRAllocaInstruction def) {
		this.def = def;
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitGetElementPtrInstruction(this);
	}

}
