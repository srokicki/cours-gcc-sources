package main.java.ir.instruction;

import main.java.ir.core.IRType;
import main.java.ir.core.IRValue;
import main.java.ir.core.IRVisitor;

public class IRLoadInstr extends IRInstruction {

	// Load instructions has one address operand and produces a value
	public IRLoadInstr(IRValue operand1, IRType type) {
		super();
		this.addOperand(operand1);
		this.setResult(new IRValue(type));
	}

	@Override
	public Object accept(IRVisitor v) {
		return v.visitLoadInstruction(this);
	}
}
