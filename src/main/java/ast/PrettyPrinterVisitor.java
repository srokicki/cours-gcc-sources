package main.java.ast;

import org.antlr.v4.runtime.tree.ParseTree;

import main.java.antlr.SimpleCBaseVisitor;
import main.java.antlr.SimpleCParser;

public class PrettyPrinterVisitor extends SimpleCBaseVisitor<String> {

	@Override
	public String visitTranslationUnit(SimpleCParser.TranslationUnitContext ctx) {
		String result = "";
		for (ParseTree c : ctx.children)
			result = result + "\n" + this.visit(c);
		return result;
	}

	@Override
	public String visitFunctionDefinition(SimpleCParser.FunctionDefinitionContext ctx) {
		String result = this.visit(ctx.returnType) + " " + ctx.name.getText() + "(";

		if (!ctx.args.isEmpty()) {
			int num_args = ctx.args.size();
			for (ParseTree c : ctx.args.subList(0, num_args - 1))
				result += this.visit(c) + ", ";
			result += this.visit(ctx.args.get(num_args - 1));
		}
		result += ")";

		return result + " {\n" + this.visit(ctx.body) + "\n}\n";

	}

	@Override
	public String visitFunctionArgument(SimpleCParser.FunctionArgumentContext ctx) {
		String result = this.visit(ctx.argType) + " " + ctx.name.getText();
		if (ctx.size != null)
			result += "[" + ctx.size.getText() + "]";
		return result;
	}

	@Override
	public String visitVoidType(SimpleCParser.VoidTypeContext ctx) {
		return "void";
	}

	@Override
	public String visitIntType(SimpleCParser.IntTypeContext ctx) {
		return "int";
	}

	@Override
	public String visitUintType(SimpleCParser.UintTypeContext ctx) {
		return "unsigned int";
	}

	@Override
	public String visitStatement(SimpleCParser.StatementContext ctx) {
		return "";
	}

}
