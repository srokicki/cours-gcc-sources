# Travaux pratiques GCC

## Partie 1 : Implémentation du front-end

L'objectif de ce TP est d'implémenter la face avant du compilateur que nous développons pendant le module. Ce compilateur doit être capable de parser un langage proche du C, et de construire la représentation intermédiaire vue en cours.

### Téléchargement des sources

Les fichiers du projet peuvent être récupérés TODO
Pour compiler notre application, nous utilisons l'outil Gradle. Vous pouvez donc vérifier le bon fonctionnement du code fourni en lançant les commandes suivantes :

` gradle run`

Cette commande devrait générer le parser minimaliste que nous vous avons fourni, puis compiler toutes les classes Java fournis. Le compilateur est ensuite appelé sur un programme constant qu'il imprime sur la sortie standard grâce à un visiteur de l'AST.

### Génération du parser avec ANTLR

Pour ce TP, nous allons utiliser ANTLR4 pour générer le parser de notre langage proche du C. Comme nous l'avons vu en cours, la syntaxe est la suivante : 

```
TranslationUnit : FunctionDefinition+;

FunctionDefinition : FunctionPrototype '{' Statement '}';

FunctionPrototype : Type IDENTIFIER '(' (FunctionArgument ',')* FunctionArgument? 

FunctionArgument : Type IDENTIFIER ArraySizeSpecifier?;

ArraySizeSpecifier : '[' INTEGER ']';

Type : 'void' | 'int' | 'unsigned int' | 'char';

Statement : BlockStatement | VarDefStatement | VarAssignStatement | IfStatement | ForStatement | WhileStatement | ExpressionStatement | ReturnStatement;

BlockStatement : '{' Statement* '}';

VarDefStatement : VarDefinition | VarArrayDefinition ';';
VarAssignStatement : ScalarAssignStatement | ArrayElementAssignStatement;

ScalarAssignStatement : IDENTIFIER '=' Expression ';';
ArrayElementAssignStatement : IDENTIFIER '[' Expression ']' '=' Expression ';';

VarDefinition : Type IDENTIFIER '=' Expression;
VarArrayDefinition : Type IDENTIFIER ArraySizeSpecifier '=' '{' (Expression ',')* Expression? 

IfStatement : 'if' '(' Expression ')' Statement ('else' Statement )?;

ForStatement : 'for' '(' VarDefinition ';' Expression ';' Expression ')' BlockStatement;

WhileStatement : 'while' '(' Expression ')' BlockStatement;

ReturnStatement : return Expression? 

ExpressionStatement : Expression ';';

Expression : Expression '+' Expression            | Expression '-' Expression            | Expression '*' Expression            | Expression '/' Expression            | Expression '%' Expression            | Expression '<' Expression            | Expression '>' Expression            | Expression '<=' Expression            | Expression '>=' Expression            | Expression '&&' Expression            | Expression '||' Expression            | '-' Expression            | '(' Expression ')'            | FunctionCall
| IDENTIFIER            | INTEGER;

FunctionCall : IDENTIFIER '(' (Expression ',')* Expression? 
```

Syntaxe de ANTLR4

La syntaxe de description d'une grammaire ANTLR4 est simple. Considérons l'exemple suivant permettant de définir des fonctions : 

```
functionDefinition : returnType=type name=IDENTIFIER '(' (args+=functionArgument ',')* args+=functionArgument? ')' '{' body=statement '}' ;
```

Sur cet exemple, il est important de noter : 
- Un symbole non terminal de la grammaire doit commencer par une minuscule (les majuscules sont associées aux règles de lexer). 
- Il est possible de nommer les occurrences d'autres symboles. Dans l'exemple précédent, il sera possible d'accéder aux champs `returnType` du noeud créé pour obtenir le noeud créé par la règle `type`. L'utilisation de cette fonctionnalité nous permet de nous rapprocher de la structure d'un AST directement dans la grammaire.
- Il est possible d'utiliser des notations de regex directement dans les productions de la grammaire (par exemple l'opérateur *)
- Il est possible de créer des listes en utilisant l'opérateur `+=`

La production suivante permet de générer les types d'une variable ou le type de retour d'une fonction :

```
type : 'void'           #VoidType
| 'int'             #IntType
| 'unsigned int'    #UintType;

```
La syntaxe ANTLR4 permet de choisir le type du noeud qui va être créé en fonction de l'alternative qui va être utilisée dans la production. Dans l'exemple précédent, si l'outil parse un `void`, il va créer un noeud de type `VoidType`. Vous avez ainsi plus de contrôle sur l'arbre que vous créez, sans avoir besoin de définir de nombreux symboles non terminaux et d'augmenter le nombre de productions.

En plus de ces exemples, nous avons fourni la grammaire permettant de décrire des fonctions vides dans le langage utilisé en TP. Celle-ci se trouve dans le fichier `src/main/java/antlr/SimpleC.g4`.

Génération du parser 

Pour générer le parser avec ANTLR4, vous pouvez lancer la commande suivante depuis le répertoire contenant le fichier `SimpleC.g4` :

``` 
antlr4 -visitor SimpleC.g4 
```

Cette commande va générer les classes correspondant aux noeuds de l'arbre de syntaxe, le lexer et le parser de notre grammaire. L'option `-visitor` permet également de générer des visiteurs permettant de parcourir l'arbre généré.

Dans le dossier `src/main/java/ast`, nous avons également fourni un exemple de visiteur permettant de régénérer le texte d'un code C que l'on vient de parser. Cet exemple vous montre comment utiliser les visiteurs générés par ANTLR.

### Modèle de la représentation intermédiaire

Dans le répertoire `src/main/java/ir`, nous vous fournissons une implémentation d'une IR basée sur les graphes de flot de contrôle et sur la forme SSA. 

```plantuml
@startuml
class IRTopLevel
class IRFunction{
String name
}
class IRBlock {
 void addTerminator(IRTerminator)
 IRTerminator getTerminator()
}
class IROperation {
 void addOperand(IRValue)
}
class IRInstruction
class IRTerminator
class IRPhiOperation

class IRValue {
 IRType type
}

IROperation <|-- IRInstruction
IROperation <|-- IRTerminator
IROperation <|-- IRPhiOperation

IRInstruction <|-- IRAddInstruction
IRInstruction <|-- IRMulInstruction
IRInstruction <|-- IRLoadInstruction
IRInstruction <|-- IRStoreInstruction
IRInstruction <|-- IRAllocaInstruction
IRInstruction <|-- IRGetElementPtrInstruction

IRTerminator <|-- IRGoto
IRTerminator <|-- IRCondBr


IRTopLevel *-- "many" IRFunction : funcs
IRFunction *-- "many" IRBlock : blocks
IRBlock o-- "many" IRBlock : preds
IRBlock *-- "many" IROperation : ops
IROperation o-- "many" IRValue : operands
IROperation *-- "1" IRValue : result
IRValue o-- "many" IROperation : uses
IRTerminator o-- "many" IRBlock : successors 

@enduml
```

L'IR permet de représenter des fonctions possédant chacune un ensemble de blocs contenant des opérations. Ces opérations peuvent être des phi-opération permettant de représenter le programme sous forme SSA, des *terminator* permettant de symboliser la fin d'un bloc et de désigner le ou les successeurs dans le graphe de flot de contrôle. Enfin, toutes les autres opérations sont des instructions, représentant tous les traitements spéciaux possibles sur des valeurs dans notre IR.

Au-delà des instructions classiques telles que l'addition ou la multiplication, nous utiliserons également des instructions dont la sémantique est moins intuitive : 
- les `IRAllocaInstruction` sont utilisées pour allouer de la mémoire sur la pile pour stocker une variable locale. Cette instruction ne renvoie rien. 
- les `IRGetElementPtrInstruction` permettent de récupérer l'adresse d'une variable. Ces instructions font référence à l'instruction d'allocation qui a créé la variable. Ils renvoient un `unsigned int` représentant une adresse mémoire (il serait possible d'ajouter un type *ptr* dans notre IR).
- les `IRLoadInstruction` et les `IRStoreInstruction` représentent la lecture et l'écriture d'une variable ou d'un tableau. L'argument représentant l'adresse est obtenu grâce à l'instruction `IRGetElementPtrInstruction` mentionnée précédemment.

Les *terminators* existent sous deux formes différentes : les `IRCondBr` qui listent deux successeurs (des blocs de l'IR) et qui utilisent un opérande pour décider quel est le successeur à prendre ; les `IRGoto` qui n'ont qu'un seul successeur et pas d'opérandes.

Toutes les opérations doivent être instanciées en utilisant le constructeur de la classe souhaitée. Les arguments de ces constructeurs aident à comprendre le fonctionnement de l'opération en question.

À l'intérieur d'un bloc de l'IR, le flot de données est encodé via les `IRValue` : chaque opération a une liste d'`IRValue` pour ses opérandes et un unique `IRValue` représentant le résultat de l'opération (il peut être *null* quand l'opération ne produit pas de valeur). Chaque `IRValue` garde la liste des opérations l'utilisant comme opérande. Pour que cette liste reste à jour, il est important de toujours ajouter les opérandes via la fonction `addOperand` des opérations. Ces opérandes sont à priori ajoutés dans les constructeurs des différentes opérations.

Enfin, le graphe de flot de contrôle à l'intérieur d'une fonction est représenté à travers les *terminators* des blocs et à travers une liste de prédécesseurs de chaque bloc. Pour que cette représentation fonctionne correctement, il est important que la dernière instruction de chaque bloc soit un `IRTerminator`. Les fonctions de navigation dans l'IR se basent sur cette hypothèse.

### Objectifs de la première partie du TP

Pour vous guider dans l'implémentation du front-end, nous vous proposons de réaliser les étapes suivantes : 
- implémentation d'une grammaire plus complète dans ANTLR4. Il n'est pas nécessaire de supporter toutes les opérations binaires et les deux types de boucles dès les premiers essais. 
- extension du `PrettyPrinter` proposé pour comprendre la structure du visiteur et de l'AST généré par ANTLR. L'idée ici est de régénérer le code que l'ont vient de parser à partir de l'AST.
- construction de la table des symboles. Pour cela il faut définir une nouvelle classe permettant de représenter les tables des symboles à plusieurs niveaux. Pour les tables de hachage, nous utiliserons les `hashmap` de java. Cette table sera remplie grâce à un visiteur sur l'AST appelant les fonctions `insert`, `lookup`, `initializeScope` et `finalizeScope` comme nous l'avons vu en cours. 
- construction du graphe de flot de contrôle ne contenant aucune instruction dans un premier temps (attention : pour bien représenter le flot, il sera malgré tout obligatoire d'ajouter les terminator dans cette représentation)
- **Facultatif** : construction d'un *pretty printer* dot permettant de visualiser le graphe de flot de contrôle afin de vérifier le bon fonctionnement de votre passe de génération de l'IR.
- modification du visiteur générant le graphe de flot de contrôle pour générer les instructions et la forme SSA en suivant l'algorithme décrit dans le cours.
- **Facultatif** : amélioration du *pretty printer* pour afficher également le graphe de flot de données à l'intérieur des blocs.
- ajout des fonctionnalités manquantes dans la grammaire et implémentation des visiteurs manquant pour que l'IR soit générée correctement

### Évaluation

L'évaluation de cette première partie sera basée sur un cours rapport décrivant les choix faits lors de l'implémentation et la stratégie adoptée pour tester le front-end. Ce rapport ne fera pas plus de deux pages et sera accompagné d'une démo du front-end pendant une séance de TP.

